// ===== Scroll to Top ====
$(window).scroll(function() {
  if ($(this).scrollTop() >= 50) { // si la pagina baja 50px
    $('#goTop').fadeIn(200); // entrada difuminandose
  } else {
    $('#goTop').fadeOut(200); // Salida difuminandose
  }
});
$('#goTop').click(function() { // Cuando se da clic
  $('body,html').animate({
    scrollTop: 0 // sube al inicio
  }, 500);
});

//Funcion para ocultar/mostrar compras del carrito

function mostrarOcultar(id) {
  var elemento = document.getElementById(id);
  if (!elemento) {
    return true;
  }
  if (elemento.style.display == "none") {
    elemento.style.display = "block"
  } else {
    elemento.style.display = "none"
  };
  return true;
};
