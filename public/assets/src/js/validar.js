function valSuscribir() {
  campo = document.getElementById('suscribir');

  valido = document.getElementById('emailOK');

  emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
  //Se muestra un texto a modo de ejemplo, luego va a ser un icono
  if (emailRegex.test(campo.value)) {
    $('#suscribir').removeClass('err');
    valido.innerText = "";
    campo.value = "";
    swal("Suscrito", "Ahora recibiras en tu correo las mejores ofertas en medicina!", "success");
  } else {
    $('#suscribir').addClass('err');
    valido.innerText = "Email inválido!";
  }
}

function valMensaje() {
  var nombre = document.contacto.nombre,
      email = document.contacto.email,
      mensaje = document.contacto.mensaje,
      valNombre = document.getElementById('nombreOK'),
      valEmail = document.getElementById('emailfOK'),
      valMensaje = document.getElementById('mensajeOK'),
      emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i,
      errorN = false,
      errorE = false,
      errorM = false;

  if (nombre.value.length > 0 ) {
    $('#nombre').removeClass('err');
    valNombre.innerText = "";
    errorN = false;
  } else {
    $('#nombre').addClass('err');
    valNombre.innerText = "Debe escribir su nombre";
    errorN = true;
  }

  if (emailRegex.test(email.value)) {
    $('#email').removeClass('err');
    valEmail.innerText = "";
    errorE = false;
  } else {
    $('#email').addClass('err');
    valEmail.innerText = "Email inválido!";
    errorE = true;
  }

  if (mensaje.value.length > 60) {
    $('#mensaje').removeClass('err');
    valMensaje.innerText = "";
    errorM = false;
  } else {
    $('#mensaje').addClass('err');
    valMensaje.innerText = "El mensaje debe de ser de al menos 60 caracteres";
    errorM = true;
  }

  if (!errorN && !errorE && !errorM) {
    nombre.value = "";
    email.value = "";
    mensaje.value = "";
    swal("Listo!", "Su mensaje a sido enviado correctamente", "success");
  }
}
