//Funcion para cargar los productos
var app = window.app || {};

  (function($) {
    'use strict';

    //no coflict con underscores

    app.init = function() {
      //totalItems totalAmount
      var total = 0,
        items = 0

      var cart = (JSON.parse(localStorage.getItem('cart')) != null)
        ? JSON.parse(localStorage.getItem('cart'))
        : {
          items: []
        };

      if (undefined != cart.items && cart.items != null && cart.items != '' && cart.items.length > 0) {
        _.forEach(cart.items, function(n, key) {
          items = (items + n.cant)
          total = total + (n.cant * n.price)
        });

      }

      $('#totalItems').text(items)
      $('.totalAmount').text('$ ' + total + ' USD')

    }
    app.createProducts = function() {
      //Se crean los productos
      var productos = [
          {
            id: 1,
            img: '/assets/img/productos/1.jpg',
            name: 'Panadol',
            price: 2.50,
            stock: 100
          }, {
            id: 2,
            img: '/assets/img/productos/2.jpg',
            name: 'Zorritone',
            price: 2.50,
            stock: 100
          }, {
            id: 3,
            img: '/assets/img/productos/3.jpg',
            name: 'Tapsin Instaflu',
            price: 2.50,
            stock: 100
          }, {
            id: 4,
            img: '/assets/img/productos/4.jpg',
            name: 'Acetaminofén',
            price: 2.50,
            stock: 100
          }, {
            id: 5,
            img: '/assets/img/productos/5.png',
            name: 'Panadol Extra Advance',
            price: 2.16,
            stock: 100
          }, {
            id: 6,
            img: '/assets/img/productos/6.png',
            name: 'Alka AD',
            price: 3.50,
            stock: 100
          }, {
            id: 7,
            img: '/assets/img/productos/7.png',
            name: 'Panadol Antigripal',
            price: 3.00,
            stock: 100
          }, {
            id: 8,
            img: '/assets/img/productos/8.png',
            name: '24/7 Burn Fórmula',
            price: 6.50,
            stock: 100
          }, {
            id: 9,
            img: '/assets/img/productos/9.jpg',
            name: 'Aspirina Forte',
            price: 3.50,
            stock: 100
          }, {
            id: 10,
            img: '/assets/img/productos/10.jpg',
            name: 'Panadol Advance',
            price: 4.50,
            stock: 100
          }, {
            id: 11,
            img: '/assets/img/productos/11.png',
            name: 'Acetaminofén MK',
            price: 1.50,
            stock: 100
          }, {
            id: 12,
            img: '/assets/img/productos/12.png',
            name: 'Antigripito',
            price: 1.75,
            stock: 100
          }, {
            id: 13,
            img: '/assets/img/productos/13.jpg',
            name: 'Artibrion',
            price: 3.50,
            stock: 100
          }, {
            id: 14,
            img: '/assets/img/productos/14.png',
            name: 'Bebetina',
            price: 2.50,
            stock: 100
          }, {
            id: 15,
            img: '/assets/img/productos/15.jpg',
            name: 'Burn 7 Capsules',
            price: 8.50,
            stock: 100
          }, {
            id: 16,
            img: '/assets/img/productos/16.png',
            name: 'Dexamigrán',
            price: 9.50,
            stock: 100
          }, {
            id: 17,
            img: '/assets/img/productos/17.png',
            name: 'Dolodol',
            price: 5.50,
            stock: 100
          }, {
            id: 18,
            img: '/assets/img/productos/18.png',
            name: 'Dolofín AR',
            price: 2.30,
            stock: 100
          }, {
            id: 19,
            img: '/assets/img/productos/19.png',
            name: 'Dolofín Flex',
            price: 2.30,
            stock: 100
          }, {
            id: 20,
            img: '/assets/img/productos/20.png',
            name: 'Paracetamol',
            price: 1.50,
            stock: 100
          }, {
            id: 21,
            img: '/assets/img/productos/21.png',
            name: 'Alka-Setzer Extreme',
            price: 4.50,
            stock: 100
          }, {
            id: 22,
            img: '/assets/img/productos/22.png',
            name: 'Foskrol Ginseng',
            price: 7.50,
            stock: 100
          }, {
            id: 23,
            img: '/assets/img/productos/23.png',
            name: 'Ultra Doceplex Mega Woman',
            price: 12.50,
            stock: 100
          }, {
            id: 24,
            img: '/assets/img/productos/24.png',
            name: 'Vick VitaPyrena',
            price: 10.00,
            stock: 100
          }, {
            id: 25,
            img: '/assets/img/productos/25.png',
            name: 'Voltaren',
            price: 15.50,
            stock: 100
          }, {
            id: 26,
            img: '/assets/img/productos/26.png',
            name: 'Viro-Grip AM',
            price: 0.50,
            stock: 100
          }, {
            id: 27,
            img: '/assets/img/productos/27.png',
            name: 'Viro-Grip Limón AM',
            price: 0.50,
            stock: 100
          }, {
            id: 28,
            img: '/assets/img/productos/28.png',
            name: 'Viro-Grip Limón PM',
            price: 0.50,
            stock: 100
          }
        ],
        wrapper = $('.productosWrapper'),
        wrapperHome = $('.productosWrapperHome'),
        contenido = ''

      for (var i = 0; i < 4; i++) {

        if (productos[i].stock > 0) {
          contenido += '<li>'
          contenido += '<a class="product-image" href="#">'
          contenido += '<img class="img-center" src="' + productos[i].img + '">'
          contenido += '</a>'
          contenido += '<h3 class="product-title">'
          contenido += '<a href="#">' + productos[i].name + '</a>'
          contenido += '</h3>'
          contenido += '<div class="product-price">$' + productos[i].price + '</div>'
          contenido += '<a type="button" class="btn icon-add ladda-button prod-' + productos[i].id + '" onclick="app.addtoCart(' + productos[i].id + ');">Agregar</a>'
          contenido += '</li>'
        }

      }
      wrapperHome.html(contenido)
      for (i = 4; i < productos.length; i++) {

        if (productos[i].stock > 0) {
          contenido += '<li>'
          contenido += '<a class="product-image" href="#">'
          contenido += '<img class="img-center" src="' + productos[i].img + '">'
          contenido += '</a>'
          contenido += '<h3 class="product-title">'
          contenido += '<a href="#">' + productos[i].name + '</a>'
          contenido += '</h3>'
          contenido += '<div class="product-price">$' + productos[i].price + '</div>'
          contenido += '<a type="button" class="btn icon-add ladda-button prod-' + productos[i].id + '" onclick="app.addtoCart(' + productos[i].id + ');">Agregar</a>'
          contenido += '</li>'
        }

      }
      wrapper.html(contenido)
      localStorage.setItem('productos', JSON.stringify(productos))
    }

    app.addtoCart = function(id) {
      var l = Ladda.create(document.querySelector('.prod-' + id));

      l.start();
      var productos = JSON.parse(localStorage.getItem('productos')),
        producto = _.find(productos, {'id': id}),
        cant = 1
      if (cant <= producto.stock) {
        if (undefined != producto) {
          if (cant > 0) {
            setTimeout(function() {
              var cart = (JSON.parse(localStorage.getItem('cart')) != null)
                ? JSON.parse(localStorage.getItem('cart'))
                : {
                  items: []
                };
              app.searchProd(cart, producto.id, parseInt(cant), producto.name, producto.price, producto.img, producto.stock)
              l.stop();
            }, 2000)
          } else {
            alert('Solo se permiten cantidades mayores a cero')
          }
        } else {
          alert('Oops! algo malo ocurrió, inténtalo de nuevo más tarde')
        }
      } else {
        alert('No se pueden añadir más de este producto')
      }
    }

    app.searchProd = function(cart, id, cant, name, price, img, available) {
      //si le pasamos un valor negativo a la cantidad, se descuenta del carrito
      var curProd = _.find(cart.items, {'id': id})

      if (undefined != curProd && curProd != null) {
        //ya existe el producto, aÃ±adimos uno mÃ¡s a su cantidad
        if (curProd.cant < available) {
          curProd.cant = parseInt(curProd.cant + cant)
        } else {
          alert('No se pueden añadir más de este producto')
        }

      } else {
        //sino existe lo agregamos al carrito
        var prod = {
          id: id,
          cant: cant,
          name: name,
          price: price,
          img: img,
          available: available
        }
        cart.items.push(prod)

      }
      localStorage.setItem('cart', JSON.stringify(cart))
      app.init()
      app.getProducts()
    }

    app.getProducts = function() {
      var cart = (JSON.parse(localStorage.getItem('cart')) != null)
          ? JSON.parse(localStorage.getItem('cart'))
          : {
            items: []
          },
        msg = '',
        wrapper = $('.cart'),
        total = 0
      wrapper.html('')

      if (undefined == cart || null == cart || cart == '' || cart.items.length == 0) {
        wrapper.html('<li>Tu canasta está vacía</li>');
        $('.cart').css('left', '-400%')
      } else {
        var items = '';
        _.forEach(cart.items, function(n, key) {

          total = total + (n.cant * n.price)
          items += '<li>'
          items += '<img src="' + n.img + '" />'
          items += '<h3 class="title">' + n.name + '<br><span class="price">' + n.cant + ' x $ ' + n.price + ' USD</span> <button onclick="app.updateItem(' + n.id + ',' + n.available + ')" title="-" style="color: #fff; line-height:20px; border: 0px; top: 5px; right: 5px; border-radius: 10px;width: 20px;height: 20px;background-color: #25A187;">-</button> <button onclick="app.deleteProd(' + n.id + ')" title="x" style="color: #fff;  top: 10px; right: 15px; border: 0px;  line-height:20px; border-radius: 10px;width: 20px;height: 20px;background-color: #25A187;">x</button><div class="clearfix"></div></h3>'
          items += '</li>'
        });

        //agregar el total al carrito
        items += '<li id="total">Total : $ ' + total + ' USD <div id="submitForm"></div></li>'
        wrapper.html(items)
        $('.cart').css('left', '-500%')
      }
    }

    app.updateItem = function(id, available) {
      //resta uno a la cantidad del carrito de compras
      var cart = (JSON.parse(localStorage.getItem('cart')) != null)
          ? JSON.parse(localStorage.getItem('cart'))
          : {
            items: []
          },
        curProd = _.find(cart.items, {'id': id})
      //actualizar el carrito
      curProd.cant = curProd.cant - 1;
      //validar que la cantidad no sea menor a 0
      if (curProd.cant > 0) {
        localStorage.setItem('cart', JSON.stringify(cart))
        app.init()
        app.getProducts()
      } else {
        app.deleteProd(id, true)
      }
    }

    app.delete = function(id) {
      var cart = (JSON.parse(localStorage.getItem('cart')) != null)
        ? JSON.parse(localStorage.getItem('cart'))
        : {
          items: []
        };
      var curProd = _.find(cart.items, {'id': id})
      _.remove(cart.items, curProd);
      localStorage.setItem('cart', JSON.stringify(cart))
      app.init()
      app.getProducts()
    }

    app.deleteProd = function(id, remove) {
      if (undefined != id && id > 0) {

        if (remove == true) {
          app.delete(id)
        } else {
          var conf = confirm('¿Deseas eliminar este producto?')
          if (conf) {
            app.delete(id)
          }
        }

      }
    }

    $(document).ready(function() {
      app.init()
      app.getProducts()
      app.createProducts()
    })

  })(jQuery)
