function initMap() {
  var map_canvas = document.getElementById('map');
  var map_options = {
    zoom: 18,
    center: new google.maps.LatLng(13.985286, -89.676975),
    mapTypeId: 'roadmap'
  };
  var map = new google.maps.Map(map_canvas, map_options);

  var contentString = '<div id="contenido">' +
  '<div id="nombre">' +
  '</div>' +
  '<h4 id="firstHeading" class="blue-title">Farmacia El Divino Maestro</h4>' +
  '</div>';

  var info = new google.maps.InfoWindow({content: contentString});

  var marker = new google.maps.Marker({
    map: map,
    animation: google.maps.Animation.DROP,
    position: new google.maps.LatLng(13.985286, -89.676975),
    title: "Drag me!"
  });
  marker.addListener('click', function() {
    info.open(map, marker);
  });
}
