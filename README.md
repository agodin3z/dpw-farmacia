![Build Status](https://gitlab.com/agodin3z/dpw-farmacia/badges/master/build.svg)

---

## Proyecto de Módulo, Diseño de Paginas Web - 2017

**WebSite para una Farmacia ~~con módulo de tienda~~**

## Antes de comenzar, los prerrequisitos son:  
Tener instalado lo siguiente:

* [Git](https://git-scm.com/)
* [Node.js](http://nodejs.org/)
* [Bower Package Manager](http://bower.io/)  
* [Grunt Task Runner](http://gruntjs.com/)
* NPM (normalmente viene con node.js)

### Instalando prerrequisitos

#### Para Git

* Ir a la [página de descargas](https://git-scm.com/downloads)
* Descargar la version correspondiente
* Instalarla

#### Para Node.js

* Ir a la [página de descargas](https://nodejs.org/es/download/)
* Descargar la version correspondiente
* Instalarla

#### Para Bower

En una consola o terminal:
```bash
$ sudo npm install -g bower
```

#### Para Grunt

En una consola o terminal:
```bash
$ sudo npm install -g grunt-cli
```

## Creación del proyecto

#### Clonar el repositorio

En una terminal o consola de Git Bash:
```bash
$ git clone https://gitlab.com/agodin3z/dpw-farmacia.git
$ cd dpw-farmacia
```

## Instalando dependencias

#### Back-end
```bash
$ npm install
```
>Instalara las dependencias que estan en package.json

#### Front-end

```bash
$ bower install
```
>Componentes bower

#### Ejecutar grunt tasks
```bash
$ grunt cssmin
$ grunt uglify
```

**Nota: Lo anterior se puede hacer en un paso**
```bash
$ grunt
```
>Grunt ejecutara las tareas definidas en el archivo de configuracion Gruntfile.js

## Estructura del proyecto

    ├── public/                  - Archivos del Website
    │   ├── assets/              - Archivos Estáticos
    │   │   ├── css/             - CSS Minificado
    │   │   ├── img/             - Imágenes
    │   │   ├── js/              - JS Minificado
    │   │   ├── lib/             - Dependencias Bower
    │   │   └── src/             - Source code
    │   │       ├── css/         - Archivos CSS
    │   │       └── js/          - Archivos JS
    │   ├── views/               - Vistas HTML
    │   │       └── templates/   - Plantillas de Vistas
    │   └── index.html           - Página de Inicio
    ├── .bowerrc                 - Configuración Bower
    ├── .editorconfig            - Configuracion para Editores
    ├── .gitignore               - Archivos ignorados por Git
    ├── .gitlab-ci.yml           - Configuracion de GitLab Pages
    ├── Gruntfile.js             - Configuración Grunt
    ├── README.md                - Readme
    ├── bower.json               - Dependencias Front-end
    └── package.json             - Componentes Back-end

## Modo de Trabajo: Forking Workflow

Crear un fork de este repositorio y enviar pull-request con los cambios que para incluirlos al proyecto.

## Participantes en el proyecto

* Alex Chinchilla
* Marcelo Hernández
* Andrés Godínez
* Álvaro Palacios
