//
module.exports = function(grunt){
  grunt.initConfig({
    pkg : grunt.file.readJSON('package.json'),
    uglify: {
      files: {
        cwd: './public/assets/src/js/',
        src: ['*.js', '!*.min.js'],
        dest: './public/assets/js/',
        expand: true,
        flatten: true,
        ext: '.min.js'
      }
    },
    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: './public/assets/src/css/',
          src: ['*.css', '!*.min.css'],
          dest: './public/assets/css/',
          ext: '.min.css'
        },
        {
          expand: true,
          cwd: './public/assets/lib/normalize-css/',
          src: ['*.css', '!*.min.css'],
          dest: './public/assets/css/',
          ext: '.min.css'
        }]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  //creando tarea por defecto
  grunt.registerTask(
    'default',
    [
      'uglify',
      'cssmin',
    ]
  );
};
